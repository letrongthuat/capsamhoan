﻿<?php require_once('../Connections/capsamhoan_conn.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_capsamhoan_conn = new KT_connection($capsamhoan_conn, $database_capsamhoan_conn);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("TenSP_vi", true, "text", "", "", "", "Vui lòng nhập vào tên sản phẩm tiếng Việt.");
$formValidation->addField("TenSP_en", true, "text", "", "", "", "Vui lòng nhập vào tên sản phẩm tiếng Anh.");
$formValidation->addField("Size", true, "double", "", "", "", "Vui lòng nhập vào kích thước sản phẩm.");
$formValidation->addField("Origin", true, "text", "", "", "", "Vui lòng nhập xuất xứ sản phẩm.");
$formValidation->addField("Price", true, "double", "", "", "", "Vui lòng nhập vào giá sản phẩm.");
$formValidation->addField("Img", true, "", "", "", "", "Vui lòng chọn hình sản phẩm.");
$formValidation->addField("idCL", true, "numeric", "", "", "", "Vui lòng chọn chủng loại sản phẩm.");
$formValidation->addField("idLoai", true, "numeric", "", "", "", "Vui lòng chọn loại sản phẩm.");
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_capsamhoan_conn, $capsamhoan_conn);
$query_Recordset1 = "SELECT TenCL_vi, id FROM chungloai ORDER BY TenCL_vi";
$Recordset1 = mysql_query($query_Recordset1, $capsamhoan_conn) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

mysql_select_db($database_capsamhoan_conn, $capsamhoan_conn);
$query_Recordset2 = "SELECT TenLoai_vi, id FROM loai ORDER BY TenLoai_vi";
$Recordset2 = mysql_query($query_Recordset2, $capsamhoan_conn) or die(mysql_error());
$row_Recordset2 = mysql_fetch_assoc($Recordset2);
$totalRows_Recordset2 = mysql_num_rows($Recordset2);

// Make an insert transaction instance
$ins_sanpham = new tNG_multipleInsert($conn_capsamhoan_conn);
$tNGs->addTransaction($ins_sanpham);
// Register triggers
$ins_sanpham->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_sanpham->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_sanpham->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$ins_sanpham->setTable("sanpham");
$ins_sanpham->addColumn("TenSP_vi", "STRING_TYPE", "POST", "TenSP_vi");
$ins_sanpham->addColumn("TenSP_en", "STRING_TYPE", "POST", "TenSP_en");
$ins_sanpham->addColumn("Alias", "STRING_TYPE", "POST", "Alias");
$ins_sanpham->addColumn("Size", "DOUBLE_TYPE", "POST", "Size");
$ins_sanpham->addColumn("Color", "STRING_TYPE", "POST", "Color");
$ins_sanpham->addColumn("Origin", "STRING_TYPE", "POST", "Origin");
$ins_sanpham->addColumn("Price", "DOUBLE_TYPE", "POST", "Price");
$ins_sanpham->addColumn("Img", "FILE_TYPE", "FILES", "Img");
$ins_sanpham->addColumn("idCL", "NUMERIC_TYPE", "POST", "idCL");
$ins_sanpham->addColumn("idLoai", "NUMERIC_TYPE", "POST", "idLoai");
$ins_sanpham->addColumn("GhiChu", "STRING_TYPE", "POST", "GhiChu");
$ins_sanpham->setPrimaryKey("id", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_sanpham = new tNG_multipleUpdate($conn_capsamhoan_conn);
$tNGs->addTransaction($upd_sanpham);
// Register triggers
$upd_sanpham->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_sanpham->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_sanpham->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$upd_sanpham->setTable("sanpham");
$upd_sanpham->addColumn("TenSP_vi", "STRING_TYPE", "POST", "TenSP_vi");
$upd_sanpham->addColumn("TenSP_en", "STRING_TYPE", "POST", "TenSP_en");
$upd_sanpham->addColumn("Alias", "STRING_TYPE", "POST", "Alias");
$upd_sanpham->addColumn("Size", "DOUBLE_TYPE", "POST", "Size");
$upd_sanpham->addColumn("Color", "STRING_TYPE", "POST", "Color");
$upd_sanpham->addColumn("Origin", "STRING_TYPE", "POST", "Origin");
$upd_sanpham->addColumn("Price", "DOUBLE_TYPE", "POST", "Price");
$upd_sanpham->addColumn("Img", "FILE_TYPE", "FILES", "Img");
$upd_sanpham->addColumn("idCL", "NUMERIC_TYPE", "POST", "idCL");
$upd_sanpham->addColumn("idLoai", "NUMERIC_TYPE", "POST", "idLoai");
$upd_sanpham->addColumn("GhiChu", "STRING_TYPE", "POST", "GhiChu");
$upd_sanpham->setPrimaryKey("id", "NUMERIC_TYPE", "GET", "id");

// Make an instance of the transaction object
$del_sanpham = new tNG_multipleDelete($conn_capsamhoan_conn);
$tNGs->addTransaction($del_sanpham);
// Register triggers
$del_sanpham->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_sanpham->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$del_sanpham->setTable("sanpham");
$del_sanpham->setPrimaryKey("id", "NUMERIC_TYPE", "GET", "id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rssanpham = $tNGs->getRecordset("sanpham");
$row_rssanpham = mysql_fetch_assoc($rssanpham);
$totalRows_rssanpham = mysql_num_rows($rssanpham);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.:Quan tri:.</title>
<script src="js/jquery.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" /><link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" /><script src="../includes/common/js/base.js" type="text/javascript"></script><script src="../includes/common/js/utility.js" type="text/javascript"></script><script src="../includes/skins/style.js" type="text/javascript"></script><?php echo $tNGs->displayValidationRules();?><script src="../includes/nxt/scripts/form.js" type="text/javascript"></script><script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script><script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: true,
  show_as_grid: true,
  merge_down_value: true
}
</script>
</head>

<body>
	<div id="header">
    	<?php include("include/head.php"); ?>
    </div><!-- end #header-->
    
    <div id="wrap-navi">
    <div id="navi">
    	<?php //include("include/menu_ngang.php"); ?>
       <!-- <div id="search">Search</div>-->
    </div><!--end #navi-->
    </div><!-- end #wrap-navi-->
    <div id="content">
    <div id="flash">
	<?php //include("include/header_flash.php"); ?>        	
    </div>
    <div id="left">   
	
    	<?php include("include/menu_doc.php"); ?>
        <?php include("include/login.php"); ?>
        
    </div><!-- end #left-->
    <div id="right">
        <div id="list-sp">            
			<div class="mot-sp">
				<h1> SẢN PHẨM </h1>
				
				
<?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng" style="margin-left:20%; text-align:left;">
  <h1>
    <?php 
// Show IF Conditional region1 
if (@$_GET['id'] == "") {
?>
    <?php echo NXT_getResource("Thêm"); ?>
    <?php 
// else Conditional region1
} else { ?>
    <?php echo NXT_getResource("Cập nhật"); ?>
    <?php } 
// endif Conditional region1
?>
    sản phẩm </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" enctype="multipart/form-data">
      <?php $cnt1 = 0; ?>
      <?php do { ?>
      <?php $cnt1++; ?>
      <?php 
// Show IF Conditional region1 
if (@$totalRows_rssanpham > 1) {
?>
      <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
      <?php } 
// endif Conditional region1
?>
      <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <tr>
          <td class="KT_th"><label for="TenSP_vi_<?php echo $cnt1; ?>">Tên sản phẩm tiếng Việt:</label></td>
          <td><input type="text" name="TenSP_vi_<?php echo $cnt1; ?>" id="TenSP_vi_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rssanpham['TenSP_vi']); ?>" size="32" maxlength="255" />
              <?php echo $tNGs->displayFieldHint("TenSP_vi");?> <?php echo $tNGs->displayFieldError("sanpham", "TenSP_vi", $cnt1); ?> </td>
        </tr>
        <tr>
          <td class="KT_th"><label for="TenSP_en_<?php echo $cnt1; ?>">Tên sản phẩm tiếng Anh:</label></td>
          <td><input type="text" name="TenSP_en_<?php echo $cnt1; ?>" id="TenSP_en_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rssanpham['TenSP_en']); ?>" size="32" maxlength="255" />
              <?php echo $tNGs->displayFieldHint("TenSP_en");?> <?php echo $tNGs->displayFieldError("sanpham", "TenSP_en", $cnt1); ?> </td>
        </tr>
        <tr>
          <td class="KT_th"><label for="Alias_<?php echo $cnt1; ?>">Alias:</label></td>
          <td><input type="text" name="Alias_<?php echo $cnt1; ?>" id="Alias_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rssanpham['Alias']); ?>" size="32" maxlength="255" />
              <?php echo $tNGs->displayFieldHint("Alias");?> <?php echo $tNGs->displayFieldError("sanpham", "Alias", $cnt1); ?> </td>
        </tr>
        <tr>
          <td class="KT_th"><label for="Size_<?php echo $cnt1; ?>">Kích thước:</label></td>
          <td><input type="text" name="Size_<?php echo $cnt1; ?>" id="Size_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rssanpham['Size']); ?>" size="32" />
              <?php echo $tNGs->displayFieldHint("Size");?> <?php echo $tNGs->displayFieldError("sanpham", "Size", $cnt1); ?> </td>
        </tr>
        <tr>
          <td class="KT_th"><label for="Color_<?php echo $cnt1; ?>">Màu:</label></td>
          <td><input type="text" name="Color_<?php echo $cnt1; ?>" id="Color_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rssanpham['Color']); ?>" size="32" maxlength="50" />
              <?php echo $tNGs->displayFieldHint("Color");?> <?php echo $tNGs->displayFieldError("sanpham", "Color", $cnt1); ?> </td>
        </tr>
        <tr>
          <td class="KT_th"><label for="Origin_<?php echo $cnt1; ?>">Xuất xứ:</label></td>
          <td><input type="text" name="Origin_<?php echo $cnt1; ?>" id="Origin_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rssanpham['Origin']); ?>" size="32" maxlength="50" />
              <?php echo $tNGs->displayFieldHint("Origin");?> <?php echo $tNGs->displayFieldError("sanpham", "Origin", $cnt1); ?> </td>
        </tr>
        <tr>
          <td class="KT_th"><label for="Price_<?php echo $cnt1; ?>">Giá:</label></td>
          <td><input type="text" name="Price_<?php echo $cnt1; ?>" id="Price_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rssanpham['Price']); ?>" size="32" />
              <?php echo $tNGs->displayFieldHint("Price");?> <?php echo $tNGs->displayFieldError("sanpham", "Price", $cnt1); ?> </td>
        </tr>
        <tr>
          <td class="KT_th"><label for="Img_<?php echo $cnt1; ?>">Hình:</label></td>
          <td><input type="file" name="Img_<?php echo $cnt1; ?>" id="Img_<?php echo $cnt1; ?>" size="32" />
              <?php echo $tNGs->displayFieldError("sanpham", "Img", $cnt1); ?> </td>
        </tr>
        
        
        <script>/*
		$(document).ready(function(){
			var x = document.getElementById("idCL_<?php echo $cnt1; ?>").selectedIndex;
			alert($("document.getElementsByTagName('option')[x].value"));
		});*/
		
		function showLoaiSP(str)
		{
		if (str=="")
		  {
		  document.getElementById("idLoai_<?php echo $cnt1; ?>").innerHTML="";
		  return;
		  } 
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
			document.getElementById("idLoai_<?php echo $cnt1; ?>").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET","ajax_loai_theo_chungloai.php?q="+str,true);
		xmlhttp.send();
		}
		</script>
        
        
        <tr>
          <td class="KT_th"><label for="idCL_<?php echo $cnt1; ?>">Chủng loại:</label></td>
          <td><div id="state_list">
 			<select onchange="showLoaiSP(this.value)" name="idCL_<?php echo $cnt1; ?>" id="idCL_<?php echo $cnt1; ?>">
            <option value=""><?php echo NXT_getResource("Chọn một..."); ?></option>
            <?php 
do {  
?>
            <option value="<?php echo $row_Recordset1['id']?>"<?php if (!(strcmp($row_Recordset1['id'], $row_rssanpham['idCL']))) {echo "SELECTED";} ?>><?php echo $row_Recordset1['TenCL_vi']?></option>
            <?php
} while ($row_Recordset1 = mysql_fetch_assoc($Recordset1));
  $rows = mysql_num_rows($Recordset1);
  if($rows > 0) {
      mysql_data_seek($Recordset1, 0);
	  $row_Recordset1 = mysql_fetch_assoc($Recordset1);
  }
?>
          </select>
              <?php echo $tNGs->displayFieldError("sanpham", "idCL", $cnt1); ?> </td>
        </tr>
        <tr>
          <td class="KT_th"><label for="idLoai_<?php echo $cnt1; ?>">Loại:</label></td>
          <td>
          <select name="idLoai_<?php echo $cnt1; ?>" id="idLoai_<?php echo $cnt1; ?>">
            <option value=""><?php echo NXT_getResource("Chọn một..."); ?></option>
            
          </select>
              <?php echo $tNGs->displayFieldError("sanpham", "idLoai", $cnt1); ?> </td>
        </tr>
        <tr>
          <td class="KT_th"><label for="GhiChu_<?php echo $cnt1; ?>">Ghi chú:</label></td>
          <td><input type="text" name="GhiChu_<?php echo $cnt1; ?>" id="GhiChu_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rssanpham['GhiChu']); ?>" size="32" maxlength="255" />
              <?php echo $tNGs->displayFieldHint("GhiChu");?> <?php echo $tNGs->displayFieldError("sanpham", "GhiChu", $cnt1); ?> </td>
        </tr>
      </table>
      <input type="hidden" name="kt_pk_sanpham_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rssanpham['kt_pk_sanpham']); ?>" />
      <?php } while ($row_rssanpham = mysql_fetch_assoc($rssanpham)); ?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1
      if (@$_GET['id'] == "") {
      ?>
          <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Thêm"); ?>" />
          <?php 
      // else Conditional region1
      } else { ?>
          <div class="KT_operations">
            <input type="submit" name="KT_Insert1" value="<?php echo NXT_getResource("Thêm mới"); ?>" onclick="nxt_form_insertasnew(this, 'id')" />
          </div>
          <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Cập nhật"); ?>" />
          <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Xóa"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
          <?php }
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Hủy"); ?>" onclick="return UNI_navigateCancel(event, '../includes/nxt/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
</br></br>
			</div><!--end #mot-sp-->                
						
		</div><!--end #list-sp-->

		<div id="phan-trang">
			
		</div><!--end #phan-trang-->
    </div><!-- end #right-->
		
    <div class="clear"></div>
		
    <div id="footer">
		<?php include("include/footer.php"); ?>
    </div><!--end #footer>
    </div><!--end #content -->
   
</body>
</html>
<?php
mysql_free_result($Recordset1);

mysql_free_result($Recordset2);
?>
