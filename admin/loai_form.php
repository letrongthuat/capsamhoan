﻿<?php require_once('../Connections/capsamhoan_conn.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_capsamhoan_conn = new KT_connection($capsamhoan_conn, $database_capsamhoan_conn);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("TenLoai_vi", true, "text", "", "", "", "Vui lòng nhập vào tên chủng loại tiếng Việt.");
$formValidation->addField("TenLoai_en", true, "text", "", "", "", "Vui lòng nhập vào tên chủng loại tiếng Anh.");
$formValidation->addField("idCL", true, "numeric", "", "", "", "Vui lòng chọn chủng loại sản phẩm.");
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_capsamhoan_conn, $capsamhoan_conn);
$query_Recordset1 = "SELECT TenCL_vi, id FROM chungloai ORDER BY TenCL_vi";
$Recordset1 = mysql_query($query_Recordset1, $capsamhoan_conn) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

// Make an insert transaction instance
$ins_loai = new tNG_multipleInsert($conn_capsamhoan_conn);
$tNGs->addTransaction($ins_loai);
// Register triggers
$ins_loai->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_loai->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_loai->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$ins_loai->setTable("loai");
$ins_loai->addColumn("TenLoai_vi", "STRING_TYPE", "POST", "TenLoai_vi");
$ins_loai->addColumn("TenLoai_en", "STRING_TYPE", "POST", "TenLoai_en");
$ins_loai->addColumn("Alias", "STRING_TYPE", "POST", "Alias");
$ins_loai->addColumn("idCL", "NUMERIC_TYPE", "POST", "idCL");
$ins_loai->addColumn("GhiChu", "STRING_TYPE", "POST", "GhiChu");
$ins_loai->setPrimaryKey("id", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_loai = new tNG_multipleUpdate($conn_capsamhoan_conn);
$tNGs->addTransaction($upd_loai);
// Register triggers
$upd_loai->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_loai->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_loai->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$upd_loai->setTable("loai");
$upd_loai->addColumn("TenLoai_vi", "STRING_TYPE", "POST", "TenLoai_vi");
$upd_loai->addColumn("TenLoai_en", "STRING_TYPE", "POST", "TenLoai_en");
$upd_loai->addColumn("Alias", "STRING_TYPE", "POST", "Alias");
$upd_loai->addColumn("idCL", "NUMERIC_TYPE", "POST", "idCL");
$upd_loai->addColumn("GhiChu", "STRING_TYPE", "POST", "GhiChu");
$upd_loai->setPrimaryKey("id", "NUMERIC_TYPE", "GET", "id");

// Make an instance of the transaction object
$del_loai = new tNG_multipleDelete($conn_capsamhoan_conn);
$tNGs->addTransaction($del_loai);
// Register triggers
$del_loai->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_loai->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$del_loai->setTable("loai");
$del_loai->setPrimaryKey("id", "NUMERIC_TYPE", "GET", "id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsloai = $tNGs->getRecordset("loai");
$row_rsloai = mysql_fetch_assoc($rsloai);
$totalRows_rsloai = mysql_num_rows($rsloai);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.:Quan tri:.</title>
<script src="js/jquery.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: true,
  show_as_grid: true,
  merge_down_value: true
}
</script>
</head>

<body>
	<div id="header">
    	<?php include("include/head.php"); ?>
    </div><!-- end #header-->
    
    <div id="wrap-navi">
    <div id="navi">
    	<?php //include("include/menu_ngang.php"); ?>
       <!-- <div id="search">Search</div>-->
    </div><!--end #navi-->
    </div><!-- end #wrap-navi-->
    <div id="content">
    <div id="flash">
	<?php //include("include/header_flash.php"); ?>        	
    </div>
    <div id="left">   
	
    	<?php include("include/menu_doc.php"); ?>
        <?php include("include/login.php"); ?>
        
    </div><!-- end #left-->
    <div id="right">
        <div id="list-sp">            
			<div class="mot-sp">
				<h1> LOẠI SẢN PHẨM </h1>
				
				
                <?php
	echo $tNGs->getErrorMsg();
?>
                <div class="KT_tng" style="margin-left:25%; text-align:left;">
                  <h1>
                    <?php 
// Show IF Conditional region1 
if (@$_GET['id'] == "") {
?>
                      <?php echo NXT_getResource("Thêm"); ?>
                      <?php 
// else Conditional region1
} else { ?>
                      <?php echo NXT_getResource("Cập nhật"); ?>
                      <?php } 
// endif Conditional region1
?>
loại	                     </h1>
                  <div class="KT_tngform">
                    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
                      <?php $cnt1 = 0; ?>
                      <?php do { ?>
                        <?php $cnt1++; ?>
                        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsloai > 1) {
?>
                          <h2><?php echo NXT_getResource("dòng"); ?> <?php echo $cnt1; ?></h2>
                          <?php } 
// endif Conditional region1
?>
                        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
                          <tr>
                            <td class="KT_th"><label for="TenLoai_vi_<?php echo $cnt1; ?>">Tên loại bằng tiếng Việt:</label></td>
                            <td><input type="text" name="TenLoai_vi_<?php echo $cnt1; ?>" id="TenLoai_vi_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsloai['TenLoai_vi']); ?>" size="32" maxlength="255" />
                                <?php echo $tNGs->displayFieldHint("TenLoai_vi");?> <?php echo $tNGs->displayFieldError("loai", "TenLoai_vi", $cnt1); ?> </td>
                          </tr>
                          <tr>
                            <td class="KT_th"><label for="TenLoai_en_<?php echo $cnt1; ?>">Tên loại bằng tiếng Anh:</label></td>
                            <td><input type="text" name="TenLoai_en_<?php echo $cnt1; ?>" id="TenLoai_en_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsloai['TenLoai_en']); ?>" size="32" maxlength="255" />
                                <?php echo $tNGs->displayFieldHint("TenLoai_en");?> <?php echo $tNGs->displayFieldError("loai", "TenLoai_en", $cnt1); ?> </td>
                          </tr>
                          <tr>
                            <td class="KT_th"><label for="Alias_<?php echo $cnt1; ?>">Alias:</label></td>
                            <td><input type="text" name="Alias_<?php echo $cnt1; ?>" id="Alias_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsloai['Alias']); ?>" size="32" maxlength="255" />
                                <?php echo $tNGs->displayFieldHint("Alias");?> <?php echo $tNGs->displayFieldError("loai", "Alias", $cnt1); ?> </td>
                          </tr>
                          <tr>
                            <td class="KT_th"><label for="idCL_<?php echo $cnt1; ?>">Chủng loại:</label></td>
                            <td><select name="idCL_<?php echo $cnt1; ?>" id="idCL_<?php echo $cnt1; ?>">
                                <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                                <?php 
do {  
?>
                                <option value="<?php echo $row_Recordset1['id']?>"<?php if (!(strcmp($row_Recordset1['id'], $row_rsloai['idCL']))) {echo "SELECTED";} ?>><?php echo $row_Recordset1['TenCL_vi']?></option>
                                <?php
} while ($row_Recordset1 = mysql_fetch_assoc($Recordset1));
  $rows = mysql_num_rows($Recordset1);
  if($rows > 0) {
      mysql_data_seek($Recordset1, 0);
	  $row_Recordset1 = mysql_fetch_assoc($Recordset1);
  }
?>
                              </select>
                                <?php echo $tNGs->displayFieldError("loai", "idCL", $cnt1); ?> </td>
                          </tr>
                          <tr>
                            <td class="KT_th"><label for="GhiChu_<?php echo $cnt1; ?>">Ghi chú:</label></td>
                            <td><input type="text" name="GhiChu_<?php echo $cnt1; ?>" id="GhiChu_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsloai['GhiChu']); ?>" size="32" maxlength="255" />
                                <?php echo $tNGs->displayFieldHint("GhiChu");?> <?php echo $tNGs->displayFieldError("loai", "GhiChu", $cnt1); ?> </td>
                          </tr>
                        </table>
                        <input type="hidden" name="kt_pk_loai_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsloai['kt_pk_loai']); ?>" />
                        <?php } while ($row_rsloai = mysql_fetch_assoc($rsloai)); ?>
                      <div class="KT_bottombuttons">
                        <div>
                          <?php 
      // Show IF Conditional region1
      if (@$_GET['id'] == "") {
      ?>
                            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Thêm"); ?>" />
                            <?php 
      // else Conditional region1
      } else { ?>
                            <div class="KT_operations">
                              <input type="submit" name="KT_Insert1" value="<?php echo NXT_getResource("Thêm mới"); ?>" onclick="nxt_form_insertasnew(this, 'id')" />
                            </div>
                            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Cập nhật"); ?>" />
                            <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Xóa"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
                            <?php }
      // endif Conditional region1
      ?>
                          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Hủy"); ?>" onclick="return UNI_navigateCancel(event, '../includes/nxt/back.php')" />
                        </div>
                      </div>
                    </form>
                  </div>
                  <br class="clearfixplain" />
                </div>
                <p>&nbsp;</p>
              </br></br>
			</div><!--end #mot-sp-->                
						
		</div><!--end #list-sp-->

		<div id="phan-trang">
			
		</div><!--end #phan-trang-->
    </div><!-- end #right-->
		
    <div class="clear"></div>
		
    <div id="footer">
		<?php include("include/footer.php"); ?>
    </div><!--end #footer>
    </div><!--end #content -->
   
</body>
</html>
<?php
mysql_free_result($Recordset1);
?>
