﻿<?php require_once('../Connections/capsamhoan_conn.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the required classes
require_once('../includes/tfi/TFI.php');
require_once('../includes/tso/TSO.php');
require_once('../includes/nav/NAV.php');

// Make unified connection variable
$conn_capsamhoan_conn = new KT_connection($capsamhoan_conn, $database_capsamhoan_conn);

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// Filter
$tfi_listloai1 = new TFI_TableFilter($conn_capsamhoan_conn, "tfi_listloai1");
$tfi_listloai1->addColumn("loai.TenLoai_vi", "STRING_TYPE", "TenLoai_vi", "%");
$tfi_listloai1->addColumn("loai.TenLoai_en", "STRING_TYPE", "TenLoai_en", "%");
$tfi_listloai1->addColumn("chungloai.id", "NUMERIC_TYPE", "idCL", "=");
$tfi_listloai1->Execute();

// Sorter
$tso_listloai1 = new TSO_TableSorter("rsloai1", "tso_listloai1");
$tso_listloai1->addColumn("loai.TenLoai_vi");
$tso_listloai1->addColumn("loai.TenLoai_en");
$tso_listloai1->addColumn("chungloai.TenCL_vi");
$tso_listloai1->setDefault("loai.TenLoai_vi");
$tso_listloai1->Execute();

// Navigation
$nav_listloai1 = new NAV_Regular("nav_listloai1", "rsloai1", "../", $_SERVER['PHP_SELF'], 5);

mysql_select_db($database_capsamhoan_conn, $capsamhoan_conn);
$query_Recordset1 = "SELECT TenCL_vi, id FROM chungloai ORDER BY TenCL_vi";
$Recordset1 = mysql_query($query_Recordset1, $capsamhoan_conn) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

//NeXTenesio3 Special List Recordset
$maxRows_rsloai1 = $_SESSION['max_rows_nav_listloai1'];
$pageNum_rsloai1 = 0;
if (isset($_GET['pageNum_rsloai1'])) {
  $pageNum_rsloai1 = $_GET['pageNum_rsloai1'];
}
$startRow_rsloai1 = $pageNum_rsloai1 * $maxRows_rsloai1;

// Defining List Recordset variable
$NXTFilter_rsloai1 = "1=1";
if (isset($_SESSION['filter_tfi_listloai1'])) {
  $NXTFilter_rsloai1 = $_SESSION['filter_tfi_listloai1'];
}
// Defining List Recordset variable
$NXTSort_rsloai1 = "loai.TenLoai_vi";
if (isset($_SESSION['sorter_tso_listloai1'])) {
  $NXTSort_rsloai1 = $_SESSION['sorter_tso_listloai1'];
}
mysql_select_db($database_capsamhoan_conn, $capsamhoan_conn);

$query_rsloai1 = "SELECT loai.TenLoai_vi, loai.TenLoai_en, chungloai.TenCL_vi AS idCL, loai.id FROM loai LEFT JOIN chungloai ON loai.idCL = chungloai.id WHERE {$NXTFilter_rsloai1} ORDER BY {$NXTSort_rsloai1}";
$query_limit_rsloai1 = sprintf("%s LIMIT %d, %d", $query_rsloai1, $startRow_rsloai1, $maxRows_rsloai1);
$rsloai1 = mysql_query($query_limit_rsloai1, $capsamhoan_conn) or die(mysql_error());
$row_rsloai1 = mysql_fetch_assoc($rsloai1);

if (isset($_GET['totalRows_rsloai1'])) {
  $totalRows_rsloai1 = $_GET['totalRows_rsloai1'];
} else {
  $all_rsloai1 = mysql_query($query_rsloai1);
  $totalRows_rsloai1 = mysql_num_rows($all_rsloai1);
}
$totalPages_rsloai1 = ceil($totalRows_rsloai1/$maxRows_rsloai1)-1;
//End NeXTenesio3 Special List Recordset

$nav_listloai1->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.:Quan tri:.</title>
<script src="js/jquery.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" /><link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" /><script src="../includes/common/js/base.js" type="text/javascript"></script><script src="../includes/common/js/utility.js" type="text/javascript"></script><script src="../includes/skins/style.js" type="text/javascript"></script><script src="../includes/nxt/scripts/list.js" type="text/javascript"></script><script src="../includes/nxt/scripts/list.js.php" type="text/javascript"></script><script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: true,
  duplicate_navigation: true,
  row_effects: true,
  show_as_buttons: true,
  record_counter: true
}
</script><style type="text/css">
  /* Dynamic List row settings */
  .KT_col_TenLoai_vi {width:140px; overflow:hidden;}
  .KT_col_TenLoai_en {width:140px; overflow:hidden;}
  .KT_col_idCL {width:140px; overflow:hidden;}
</style>

</head>

<body>
	<div id="header">
    	<?php include("include/head.php"); ?>
    </div><!-- end #header-->
    
    <div id="wrap-navi">
    <div id="navi">
    	<?php //include("include/menu_ngang.php"); ?>
       <!-- <div id="search">Search</div>-->
    </div><!--end #navi-->
    </div><!-- end #wrap-navi-->
    <div id="content">
    <div id="flash">
	<?php //include("include/header_flash.php"); ?>        	
    </div>
    <div id="left">   
	
    	<?php include("include/menu_doc.php"); ?>
        <?php include("include/login.php"); ?>
        
    </div><!-- end #left-->
    <div id="right">
        <div id="list-sp">            
			<div class="mot-sp">
				<h1> LOẠI SẢN PHẨM </h1>
				
				
<div class="KT_tng" id="listloai1" style="text-align:left;margin-left:6%;">
  <h1> Loại sản phẩm
    <?php
  $nav_listloai1->Prepare();
  require("../includes/nav/NAV_Text_Statistics.inc.php");
?>
  </h1>
  <div class="KT_tnglist">
    <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
      <div class="KT_options"> <a href="<?php echo $nav_listloai1->getShowAllLink(); ?>"><?php echo NXT_getResource("Hiển thị"); ?>
            <?php 
  // Show IF Conditional region1
  if (@$_GET['show_all_nav_listloai1'] == 1) {
?>
            <?php echo $_SESSION['default_max_rows_nav_listloai1']; ?>
            <?php 
  // else Conditional region1
  } else { ?>
            <?php echo NXT_getResource("tất cả"); ?>
            <?php } 
  // endif Conditional region1
?>
            <?php echo NXT_getResource("dòng"); ?></a> &nbsp;
        &nbsp;
                            <?php 
  // Show IF Conditional region2
  if (@$_SESSION['has_filter_tfi_listloai1'] == 1) {
?>
                            <a href="<?php echo $tfi_listloai1->getResetFilterLink(); ?>"><?php echo NXT_getResource("Reset bộ lọc"); ?></a>
                            <?php 
  // else Conditional region2
  } else { ?>
                            <a href="<?php echo $tfi_listloai1->getShowFilterLink(); ?>"><?php echo NXT_getResource("Show bộ lọc"); ?></a>
                            <?php } 
  // endif Conditional region2
?>
      </div>
      <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <thead>
          <tr class="KT_row_order">
            <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>
            </th>
            <th id="TenLoai_vi" class="KT_sorter KT_col_TenLoai_vi <?php echo $tso_listloai1->getSortIcon('loai.TenLoai_vi'); ?>"> <a href="<?php echo $tso_listloai1->getSortLink('loai.TenLoai_vi'); ?>">Tên loại bằng tiếng Anh</a> </th>
            <th id="TenLoai_en" class="KT_sorter KT_col_TenLoai_en <?php echo $tso_listloai1->getSortIcon('loai.TenLoai_en'); ?>"> <a href="<?php echo $tso_listloai1->getSortLink('loai.TenLoai_en'); ?>">Tên loại bằng tiếng Việt</a> </th>
            <th id="idCL" class="KT_sorter KT_col_idCL <?php echo $tso_listloai1->getSortIcon('chungloai.TenCL_vi'); ?>"> <a href="<?php echo $tso_listloai1->getSortLink('chungloai.TenCL_vi'); ?>">Chủng loại</a> </th>
            <th>&nbsp;</th>
          </tr>
          <?php 
  // Show IF Conditional region3
  if (@$_SESSION['has_filter_tfi_listloai1'] == 1) {
?>
          <tr class="KT_row_filter">
            <td>&nbsp;</td>
            <td><input type="text" name="tfi_listloai1_TenLoai_vi" id="tfi_listloai1_TenLoai_vi" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listloai1_TenLoai_vi']); ?>" size="20" maxlength="255" /></td>
            <td><input type="text" name="tfi_listloai1_TenLoai_en" id="tfi_listloai1_TenLoai_en" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listloai1_TenLoai_en']); ?>" size="20" maxlength="255" /></td>
            <td><select name="tfi_listloai1_idCL" id="tfi_listloai1_idCL">
              <option value="" <?php if (!(strcmp("", @$_SESSION['tfi_listloai1_idCL']))) {echo "SELECTED";} ?>><?php echo NXT_getResource("None"); ?></option>
              <?php
do {  
?>
              <option value="<?php echo $row_Recordset1['id']?>"<?php if (!(strcmp($row_Recordset1['id'], @$_SESSION['tfi_listloai1_idCL']))) {echo "SELECTED";} ?>><?php echo $row_Recordset1['TenCL_vi']?></option>
              <?php
} while ($row_Recordset1 = mysql_fetch_assoc($Recordset1));
  $rows = mysql_num_rows($Recordset1);
  if($rows > 0) {
      mysql_data_seek($Recordset1, 0);
	  $row_Recordset1 = mysql_fetch_assoc($Recordset1);
  }
?>
            </select>
            </td>
            <td><input type="submit" name="tfi_listloai1" value="<?php echo NXT_getResource("Bộ lọc"); ?>" /></td>
          </tr>
          <?php } 
  // endif Conditional region3
?>
        </thead>
        <tbody>
          <?php if ($totalRows_rsloai1 == 0) { // Show if recordset empty ?>
          <tr>
            <td colspan="5"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
          </tr>
          <?php } // Show if recordset empty ?>
          <?php if ($totalRows_rsloai1 > 0) { // Show if recordset not empty ?>
          <?php do { ?>
          <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
            <td><input type="checkbox" name="kt_pk_loai" class="id_checkbox" value="<?php echo $row_rsloai1['id']; ?>" />
                <input type="hidden" name="id" class="id_field" value="<?php echo $row_rsloai1['id']; ?>" />
            </td>
            <td><div class="KT_col_TenLoai_vi"><?php echo KT_FormatForList($row_rsloai1['TenLoai_vi'], 20); ?></div></td>
            <td><div class="KT_col_TenLoai_en"><?php echo KT_FormatForList($row_rsloai1['TenLoai_en'], 20); ?></div></td>
            <td><div class="KT_col_idCL"><?php echo KT_FormatForList($row_rsloai1['idCL'], 20); ?></div></td>
            <td><a class="KT_edit_link" href="loai_form.php?id=<?php echo $row_rsloai1['id']; ?>&amp;KT_back=1"><?php echo NXT_getResource("Sửa"); ?></a> <a class="KT_delete_link" href="#delete"><?php echo NXT_getResource("Xóa"); ?></a> </td>
          </tr>
          <?php } while ($row_rsloai1 = mysql_fetch_assoc($rsloai1)); ?>
          <?php } // Show if recordset not empty ?>
        </tbody>
      </table>
      <div class="KT_bottomnav">
        <div>
          <?php
            $nav_listloai1->Prepare();
            require("../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
        </div>
      </div>
      <div class="KT_bottombuttons">
        <div class="KT_operations"> <a class="KT_edit_op_link" href="#" onclick="nxt_list_edit_link_form(this); return false;"><?php echo NXT_getResource("Sửa các mục đang chọn"); ?></a> <a class="KT_delete_op_link" href="#" onclick="nxt_list_delete_link_form(this); return false;"><?php echo NXT_getResource("Xóa các mục đang chọn"); ?></a> </div>
        <span>&nbsp;</span>
        <select name="no_new" id="no_new">
          <option value="1">1</option>
          <option value="3">3</option>
          <option value="6">6</option>
        </select>
        <a class="KT_additem_op_link" href="loai_form.php?KT_back=1" onclick="return nxt_list_additem(this)"><?php echo NXT_getResource("Thêm mới"); ?></a> </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
</br></br>
			</div><!--end #mot-sp-->                
						
		</div><!--end #list-sp-->

		<div id="phan-trang">
			
		</div><!--end #phan-trang-->
    </div><!-- end #right-->
		
    <div class="clear"></div>
		
    <div id="footer">
		<?php include("include/footer.php"); ?>
    </div><!--end #footer>
    </div><!--end #content -->
   
</body>
</html>
<?php
mysql_free_result($Recordset1);

mysql_free_result($rsloai1);
?>
