﻿<?php require_once('../Connections/capsamhoan_conn.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_capsamhoan_conn = new KT_connection($capsamhoan_conn, $database_capsamhoan_conn);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("TenCL_vi", true, "text", "", "", "", "Vui lòng nhập vào tên chủng loại tiếng Việt.");
$formValidation->addField("TenCL_en", true, "text", "", "", "", "Vui lòng nhập vào tên chủng loại tiếng Anh.");
$tNGs->prepareValidation($formValidation);
// End trigger

// Make an insert transaction instance
$ins_chungloai = new tNG_multipleInsert($conn_capsamhoan_conn);
$tNGs->addTransaction($ins_chungloai);
// Register triggers
$ins_chungloai->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_chungloai->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_chungloai->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$ins_chungloai->setTable("chungloai");
$ins_chungloai->addColumn("TenCL_vi", "STRING_TYPE", "POST", "TenCL_vi");
$ins_chungloai->addColumn("TenCL_en", "STRING_TYPE", "POST", "TenCL_en");
$ins_chungloai->addColumn("Alias", "STRING_TYPE", "POST", "Alias");
$ins_chungloai->addColumn("GhiChu", "STRING_TYPE", "POST", "GhiChu");
$ins_chungloai->setPrimaryKey("id", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_chungloai = new tNG_multipleUpdate($conn_capsamhoan_conn);
$tNGs->addTransaction($upd_chungloai);
// Register triggers
$upd_chungloai->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_chungloai->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_chungloai->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$upd_chungloai->setTable("chungloai");
$upd_chungloai->addColumn("TenCL_vi", "STRING_TYPE", "POST", "TenCL_vi");
$upd_chungloai->addColumn("TenCL_en", "STRING_TYPE", "POST", "TenCL_en");
$upd_chungloai->addColumn("Alias", "STRING_TYPE", "POST", "Alias");
$upd_chungloai->addColumn("GhiChu", "STRING_TYPE", "POST", "GhiChu");
$upd_chungloai->setPrimaryKey("id", "NUMERIC_TYPE", "GET", "id");

// Make an instance of the transaction object
$del_chungloai = new tNG_multipleDelete($conn_capsamhoan_conn);
$tNGs->addTransaction($del_chungloai);
// Register triggers
$del_chungloai->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_chungloai->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$del_chungloai->setTable("chungloai");
$del_chungloai->setPrimaryKey("id", "NUMERIC_TYPE", "GET", "id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rschungloai = $tNGs->getRecordset("chungloai");
$row_rschungloai = mysql_fetch_assoc($rschungloai);
$totalRows_rschungloai = mysql_num_rows($rschungloai);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.:Quan tri:.</title>
<script src="js/jquery.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: true,
  show_as_grid: true,
  merge_down_value: true
}
</script>
</head>

<body>
	<div id="header">
    	<?php include("include/head.php"); ?>
    </div><!-- end #header-->
    
    <div id="wrap-navi">
    <div id="navi">
    	<?php //include("include/menu_ngang.php"); ?>
       <!-- <div id="search">Search</div>-->
    </div><!--end #navi-->
    </div><!-- end #wrap-navi-->
    <div id="content">
    <div id="flash">
	<?php //include("include/header_flash.php"); ?>        	
    </div>
    <div id="left">   
	
    	<?php include("include/menu_doc.php"); ?>
        <?php include("include/login.php"); ?>
        
    </div><!-- end #left-->
    <div id="right">
        <div id="list-sp">            
			<div class="mot-sp">
				<h1> CHỦNG LOẠI </h1>
				
				
                <?php
	echo $tNGs->getErrorMsg();
?>
                <div class="KT_tng" style="margin-left:30%; text-align:left;">
                  <h1>
                    <?php 
// Show IF Conditional region1 
if (@$_GET['id'] == "") {
?>
                      <?php echo NXT_getResource("Thêm"); ?>
                      <?php 
// else Conditional region1
} else { ?>
                      <?php echo NXT_getResource("Cập nhật"); ?>
                      <?php } 
// endif Conditional region1
?>
                    chủng loại </h1>
                  <div class="KT_tngform">
                    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
                      <?php $cnt1 = 0; ?>
                      <?php do { ?>
                        <?php $cnt1++; ?>
                        <?php 
// Show IF Conditional region1 
if (@$totalRows_rschungloai > 1) {
?>
                          <h2><?php echo NXT_getResource("dòng"); ?> <?php echo $cnt1; ?></h2>
                          <?php } 
// endif Conditional region1
?>
                        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
                          <tr>
                            <td class="KT_th"><label for="TenCL_vi_<?php echo $cnt1; ?>">Tên tiếng Việt:</label></td>
                            <td><input type="text" name="TenCL_vi_<?php echo $cnt1; ?>" id="TenCL_vi_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rschungloai['TenCL_vi']); ?>" size="32" maxlength="255" />
                                <?php echo $tNGs->displayFieldHint("TenCL_vi");?> <?php echo $tNGs->displayFieldError("chungloai", "TenCL_vi", $cnt1); ?> </td>
                          </tr>
                          <tr>
                            <td class="KT_th"><label for="TenCL_en_<?php echo $cnt1; ?>">Tên tiếng Anh:</label></td>
                            <td><input type="text" name="TenCL_en_<?php echo $cnt1; ?>" id="TenCL_en_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rschungloai['TenCL_en']); ?>" size="32" maxlength="255" />
                                <?php echo $tNGs->displayFieldHint("TenCL_en");?> <?php echo $tNGs->displayFieldError("chungloai", "TenCL_en", $cnt1); ?> </td>
                          </tr>
                          <tr>
                            <td class="KT_th"><label for="Alias_<?php echo $cnt1; ?>">Alias:</label></td>
                            <td><input type="text" name="Alias_<?php echo $cnt1; ?>" id="Alias_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rschungloai['Alias']); ?>" size="32" maxlength="255" />
                                <?php echo $tNGs->displayFieldHint("Alias");?> <?php echo $tNGs->displayFieldError("chungloai", "Alias", $cnt1); ?> </td>
                          </tr>
                          <tr>
                            <td class="KT_th"><label for="GhiChu_<?php echo $cnt1; ?>">Ghi chú:</label></td>
                            <td><input type="text" name="GhiChu_<?php echo $cnt1; ?>" id="GhiChu_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rschungloai['GhiChu']); ?>" size="32" maxlength="255" />
                                <?php echo $tNGs->displayFieldHint("GhiChu");?> <?php echo $tNGs->displayFieldError("chungloai", "GhiChu", $cnt1); ?> </td>
                          </tr>
                        </table>
                        <input type="hidden" name="kt_pk_chungloai_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rschungloai['kt_pk_chungloai']); ?>" />
                        <?php } while ($row_rschungloai = mysql_fetch_assoc($rschungloai)); ?>
                      <div class="KT_bottombuttons">
                        <div>
                          <?php 
      // Show IF Conditional region1
      if (@$_GET['id'] == "") {
      ?>
                            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Thêm"); ?>" />
                            <?php 
      // else Conditional region1
      } else { ?>
                            <div class="KT_operations">
                              <input type="submit" name="KT_Insert1" value="<?php echo NXT_getResource("Thêm mới"); ?>" onclick="nxt_form_insertasnew(this, 'id')" />
                            </div>
                            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Cập nhật"); ?>" />
                            <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Xóa"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
                            <?php }
      // endif Conditional region1
      ?>
                          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Hủy"); ?>" onclick="return UNI_navigateCancel(event, '../includes/nxt/back.php')" />
                        </div>
                      </div>
                    </form>
                  </div>
                  <br class="clearfixplain" />
                </div>
                <p>&nbsp;</p>
              </br></br>
			</div><!--end #mot-sp-->                
						
		</div><!--end #list-sp-->

		<div id="phan-trang">
			
		</div><!--end #phan-trang-->
    </div><!-- end #right-->
		
    <div class="clear"></div>
		
    <div id="footer">
		<?php include("include/footer.php"); ?>
    </div><!--end #footer>
    </div><!--end #content -->
   
</body>
</html>
