-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: May 19, 2014 at 02:07 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `capsamhoan_db`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `chungloai`
-- 

CREATE TABLE `chungloai` (
  `id` int(11) NOT NULL auto_increment,
  `TenCL_vi` varchar(255) collate utf8_unicode_ci default NULL,
  `TenCL_en` varchar(255) collate utf8_unicode_ci default NULL,
  `Alias` varchar(255) collate utf8_unicode_ci default NULL,
  `GhiChu` varchar(255) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- 
-- Dumping data for table `chungloai`
-- 

INSERT INTO `chungloai` VALUES (1, 'Vải Cotton 100%', 'Cotton 100% Wash', 'Cotton-100-Wash', NULL);
INSERT INTO `chungloai` VALUES (2, 'Cotton Castro', 'Cotton Castro', 'Cotton-Castro', NULL);
INSERT INTO `chungloai` VALUES (3, 'Poly Acrilic', 'Poly Acrilic', 'Poly-Acrilic', NULL);
INSERT INTO `chungloai` VALUES (4, 'Hip Hop', 'Hip Hop', 'Hip-Hop', NULL);
INSERT INTO `chungloai` VALUES (5, 'Jean', 'Jean', 'Jean', NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `loai`
-- 

CREATE TABLE `loai` (
  `id` int(11) NOT NULL auto_increment,
  `TenLoai_vi` varchar(255) collate utf8_unicode_ci default NULL,
  `TenLoai_en` varchar(255) collate utf8_unicode_ci default NULL,
  `Alias` varchar(255) collate utf8_unicode_ci default NULL,
  `idCL` int(11) NOT NULL,
  `GhiChu` varchar(255) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

-- 
-- Dumping data for table `loai`
-- 

INSERT INTO `loai` VALUES (1, 'Jean Hip Hop', 'Jean Hip Hop', 'Jean-Hip-Hop', 5, NULL);
INSERT INTO `loai` VALUES (2, 'Jean Mix', 'Jean Mix', 'Jean-Mix', 5, NULL);
INSERT INTO `loai` VALUES (3, 'Hip Hop Cotton', 'Hip Hop Cotton', 'Hip-Hop-Cotton', 4, NULL);
INSERT INTO `loai` VALUES (4, 'Hip Hop Acrilic', 'Hip Hop Acrilic', 'Hip-Hop-Acrilic', 4, NULL);
INSERT INTO `loai` VALUES (5, 'Castro1', 'Castro1', 'Castro1', 2, NULL);
INSERT INTO `loai` VALUES (6, 'Acrilic 1', 'Acrilic 1', 'Acrilic-1', 3, NULL);
INSERT INTO `loai` VALUES (7, 'Cotton 1', 'Cotton 1', 'Cotton-1', 1, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `sanpham`
-- 

CREATE TABLE `sanpham` (
  `id` int(11) NOT NULL auto_increment,
  `TenSP_vi` varchar(255) collate utf8_unicode_ci default NULL,
  `TenSP_en` varchar(255) collate utf8_unicode_ci default NULL,
  `Alias` varchar(255) collate utf8_unicode_ci default NULL,
  `Size` float NOT NULL,
  `Color` varchar(50) collate utf8_unicode_ci NOT NULL,
  `Origin` varchar(50) collate utf8_unicode_ci NOT NULL,
  `Price` float NOT NULL,
  `Img` varchar(255) collate utf8_unicode_ci NOT NULL,
  `idCL` int(11) NOT NULL,
  `idLoai` int(11) NOT NULL,
  `GhiChu` varchar(255) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `sanpham`
-- 

INSERT INTO `sanpham` VALUES (1, 'MH001', 'MH001', NULL, 20, 'Đỏ', 'Nhật', 200000, '', 5, 2, NULL);
INSERT INTO `sanpham` VALUES (2, 'MH002', 'MH002', 'MH002', 30, 'Trắng', 'Hàn Quốc', 100000, '', 4, 3, NULL);
INSERT INTO `sanpham` VALUES (3, 'MH003', 'MH003', 'MH003', 40, 'Xanh', 'Hồng Kông', 10000, '', 4, 3, NULL);
INSERT INTO `sanpham` VALUES (4, 'MH004', 'MH004', 'MH004', 12, 'Xanh lá', 'Campuchia', 2000, '', 4, 3, NULL);
