-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: May 20, 2014 at 12:29 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `capsamhoan_db`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `chungloai`
-- 

CREATE TABLE `chungloai` (
  `id` int(11) NOT NULL auto_increment,
  `TenCL_vi` varchar(255) collate utf8_unicode_ci default NULL,
  `TenCL_en` varchar(255) collate utf8_unicode_ci default NULL,
  `Alias` varchar(255) collate utf8_unicode_ci default NULL,
  `GhiChu` varchar(255) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- 
-- Dumping data for table `chungloai`
-- 

INSERT INTO `chungloai` VALUES (1, 'Cotton 100%', 'Cotton 100% Wash', 'Cotton-100-Wash', NULL);
INSERT INTO `chungloai` VALUES (2, 'Cotton Castro', 'Cotton Castro', 'Cotton-Castro', NULL);
INSERT INTO `chungloai` VALUES (3, 'Poly Acrilic', 'Poly Acrilic', 'Poly-Acrilic', NULL);
INSERT INTO `chungloai` VALUES (4, 'Hip Hop', 'Hip Hop', 'Hip-Hop', NULL);
INSERT INTO `chungloai` VALUES (5, 'Jean', 'Jean', 'Jean', NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `loai`
-- 

CREATE TABLE `loai` (
  `id` int(11) NOT NULL auto_increment,
  `TenLoai_vi` varchar(255) collate utf8_unicode_ci default NULL,
  `TenLoai_en` varchar(255) collate utf8_unicode_ci default NULL,
  `Alias` varchar(255) collate utf8_unicode_ci default NULL,
  `idCL` int(11) NOT NULL,
  `GhiChu` varchar(255) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

-- 
-- Dumping data for table `loai`
-- 

INSERT INTO `loai` VALUES (1, 'Jean Hip Hop', 'Jean Hip Hop', 'Jean-Hip-Hop', 5, NULL);
INSERT INTO `loai` VALUES (2, 'Jean Mix', 'Jean Mix', 'Jean-Mix', 5, NULL);
INSERT INTO `loai` VALUES (3, 'Hip Hop Cotton', 'Hip Hop Cotton', 'Hip-Hop-Cotton', 4, NULL);
INSERT INTO `loai` VALUES (4, 'Hip Hop Acrilic', 'Hip Hop Acrilic', 'Hip-Hop-Acrilic', 4, NULL);
INSERT INTO `loai` VALUES (5, 'Castro1', 'Castro1', 'Castro1', 2, NULL);
INSERT INTO `loai` VALUES (6, 'Acrilic 1', 'Acrilic 1', 'Acrilic-1', 3, NULL);
INSERT INTO `loai` VALUES (7, 'Cotton 1', 'Cotton 1', 'Cotton-1', 1, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `sanpham`
-- 

CREATE TABLE `sanpham` (
  `id` int(11) NOT NULL auto_increment,
  `TenSP_vi` varchar(255) collate utf8_unicode_ci default NULL,
  `TenSP_en` varchar(255) collate utf8_unicode_ci default NULL,
  `Alias` varchar(255) collate utf8_unicode_ci default NULL,
  `Size` float NOT NULL,
  `Color` varchar(50) collate utf8_unicode_ci NOT NULL,
  `Origin` varchar(50) collate utf8_unicode_ci NOT NULL,
  `Price` float NOT NULL,
  `Img` varchar(255) collate utf8_unicode_ci NOT NULL,
  `idCL` int(11) NOT NULL,
  `idLoai` int(11) NOT NULL,
  `GhiChu` varchar(255) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=45 ;

-- 
-- Dumping data for table `sanpham`
-- 

INSERT INTO `sanpham` VALUES (1, 'MH001', 'MH001', NULL, 20, 'Đỏ', 'Nhật', 200000, '12-12.jpg', 5, 2, NULL);
INSERT INTO `sanpham` VALUES (2, 'MH002', 'MH002', 'MH002', 30, 'Trắng', 'Hàn Quốc', 100000, '11-11.jpg', 4, 3, NULL);
INSERT INTO `sanpham` VALUES (3, 'MH003', 'MH003', 'MH003', 40, 'Xanh', 'Hồng Kông', 10000, '1-1.jpg', 4, 3, NULL);
INSERT INTO `sanpham` VALUES (4, 'MH004', 'MH004', 'MH004', 12, 'Xanh lá', 'Campuchia', 2000, '2-2.jpg', 1, 7, NULL);
INSERT INTO `sanpham` VALUES (5, 'MH005', 'MH005', 'MH005', 80, 'Tím', 'HQ', 100, '0-0.jpg', 1, 7, NULL);
INSERT INTO `sanpham` VALUES (9, 'MH02', 'MH02', 'MH02', 20, 'Ðen', 'Việt Nam', 100000, '0-0.jpg', 1, 7, '');
INSERT INTO `sanpham` VALUES (10, 'MH03', 'MH03', 'MH03', 20, 'Ðen', 'Việt Nam', 100000, '1-1.jpg', 1, 7, '');
INSERT INTO `sanpham` VALUES (11, 'MH04', 'MH04', 'MH04', 20, 'Ðen', 'Việt Nam', 100000, '10-10.jpg', 3, 6, '');
INSERT INTO `sanpham` VALUES (12, 'MH05', 'MH05', 'MH05', 20, 'Ðen', 'Việt Nam', 100000, '11-11.jpg', 3, 6, '');
INSERT INTO `sanpham` VALUES (13, 'MH06', 'MH06', 'MH06', 20, 'Ðen', 'Việt Nam', 100000, '12-12.jpg', 2, 5, '');
INSERT INTO `sanpham` VALUES (14, 'MH07', 'MH07', 'MH07', 20, 'Ðen', 'Việt Nam', 100000, '13-13.jpg', 4, 3, '');
INSERT INTO `sanpham` VALUES (15, 'MH08', 'MH08', 'MH08', 20, 'Ðen', 'Việt Nam', 100000, '14-14.jpg', 5, 2, '');
INSERT INTO `sanpham` VALUES (16, 'MH09', 'MH09', 'MH09', 20, 'Ðen', 'Việt Nam', 100000, '18-18.jpg', 2, 5, '');
INSERT INTO `sanpham` VALUES (17, 'MH010', 'MH010', 'MH010', 20, 'Ðen', 'Việt Nam', 100000, '19-19.jpg', 5, 2, '');
INSERT INTO `sanpham` VALUES (18, 'MH011', 'MH011', 'MH011', 20, 'Ðen', 'Việt Nam', 100000, '2-2.jpg', 2, 5, '');
INSERT INTO `sanpham` VALUES (19, 'MH012', 'MH012', 'MH012', 20, 'Ðen', 'Việt Nam', 100000, '20-20.jpg', 4, 3, '');
INSERT INTO `sanpham` VALUES (20, 'MH013', 'MH013', 'MH013', 20, 'Ðen', 'Việt Nam', 100000, '21-21.jpg', 5, 2, '');
INSERT INTO `sanpham` VALUES (21, 'MH014', 'MH014', 'MH014', 20, 'Ðen', 'Việt Nam', 100000, '22-22.jpg', 4, 4, '');
INSERT INTO `sanpham` VALUES (22, 'MH015', 'MH015', 'MH015', 20, 'Ðen', 'Việt Nam', 100000, '23-23.jpg', 4, 4, '');
INSERT INTO `sanpham` VALUES (23, 'MH016', 'MH016', 'MH016', 20, 'Ðen', 'Việt Nam', 100000, '24-24.jpg', 4, 4, '');
INSERT INTO `sanpham` VALUES (24, 'MH017', 'MH017', 'MH017', 20, 'Ðen', 'Việt Nam', 100000, '25-25.jpg', 5, 2, '');
INSERT INTO `sanpham` VALUES (25, 'MH018', 'MH018', 'MH018', 20, 'Ðen', 'Việt Nam', 100000, '251-251.jpg', 4, 3, '');
INSERT INTO `sanpham` VALUES (26, 'MH019', 'MH019', 'MH019', 20, 'Ðen', 'Việt Nam', 100000, '252-252.jpg', 5, 2, '');
INSERT INTO `sanpham` VALUES (27, 'MH020', 'MH020', 'MH020', 20, 'Ðen', 'Việt Nam', 100000, '26-26.JPG', 2, 5, '');
INSERT INTO `sanpham` VALUES (28, 'MH021', 'MH021', 'MH021', 20, 'Ðen', 'Việt Nam', 100000, '27-27.JPG', 5, 2, '');
INSERT INTO `sanpham` VALUES (29, 'MH022', 'MH022', 'MH022', 20, 'Ðen', 'Việt Nam', 100000, '28-28.JPG', 2, 5, '');
INSERT INTO `sanpham` VALUES (30, 'MH023', 'MH023', 'MH023', 20, 'Ðen', 'Việt Nam', 100000, '29-29.JPG', 5, 2, '');
INSERT INTO `sanpham` VALUES (31, 'MH024', 'MH024', 'MH024', 20, 'Ðen', 'Việt Nam', 100000, '3-3.jpg', 2, 5, '');
INSERT INTO `sanpham` VALUES (32, 'MH025', 'MH025', 'MH025', 20, 'Ðen', 'Việt Nam', 100000, '4-4.jpg', 2, 5, '');
INSERT INTO `sanpham` VALUES (33, 'MH026', 'MH026', 'MH026', 20, 'Ðen', 'Việt Nam', 100000, '5-5.jpg', 2, 5, '');
INSERT INTO `sanpham` VALUES (34, 'MH027', 'MH027', 'MH027', 20, 'Ðen', 'Việt Nam', 100000, '6-6.jpg', 1, 1, '');
INSERT INTO `sanpham` VALUES (35, 'MH028', 'MH028', 'MH028', 20, 'Ðen', 'Việt Nam', 100000, '7-7.jpg', 1, 1, '');
INSERT INTO `sanpham` VALUES (36, 'MH029', 'MH029', 'MH029', 20, 'Ðen', 'Việt Nam', 100000, '8-8.jpg', 1, 1, '');
INSERT INTO `sanpham` VALUES (37, 'MH030', 'MH030', 'MH030', 20, 'Ðen', 'Việt Nam', 100000, '9-9.jpg', 1, 1, '');
INSERT INTO `sanpham` VALUES (38, 'MH031', 'MH031', 'MH031', 20, 'Ðen', 'Việt Nam', 100000, 'P1000016.JPG', 1, 1, '');
INSERT INTO `sanpham` VALUES (39, 'MH032', 'MH032', 'MH032', 20, 'Ðen', 'Việt Nam', 100000, 'P1000025.JPG', 1, 1, '');
INSERT INTO `sanpham` VALUES (40, 'MH033', 'MH033', 'MH033', 20, 'Ðen', 'Việt Nam', 100000, 'P1000026.JPG', 1, 1, '');
INSERT INTO `sanpham` VALUES (41, 'MH034', 'MH034', 'MH034', 20, 'Ðen', 'Việt Nam', 100000, 'P1000027.JPG', 1, 1, '');
INSERT INTO `sanpham` VALUES (42, 'MH035', 'MH035', 'MH035', 20, 'Ðen', 'Việt Nam', 100000, 'P1000028.JPG', 1, 1, '');
INSERT INTO `sanpham` VALUES (43, 'MH036', 'MH036', 'MH036', 20, 'Ðen', 'Việt Nam', 100000, 'P1000030.JPG', 1, 1, '');
INSERT INTO `sanpham` VALUES (44, 'MH037', 'MH037', 'MH037', 20, 'Ðen', 'Việt Nam', 100000, '___2.JPG', 1, 1, '');
